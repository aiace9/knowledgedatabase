#!/usr/bin/python3
# https://stackoverflow.com/questions/22390064/use-dbus-to-just-send-a-message-in-python

#Python DBUS Test Server
#runs until the Quit() method is called via DBUS
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop

class MyDBUSService(dbus.service.Object):
    def __init__(self):
        bus_name = dbus.service.BusName('org.my.test', bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, '/org/my/test')

    @dbus.service.method('org.my.test')
    def hello(self):
        """returns the string 'Hello, World!'"""
        return "Hello, World!"

    @dbus.service.method('org.my.test')
    def string_echo(self, s):
        """returns whatever is passed to it"""
        return s

    @dbus.service.method('org.my.test')
    def Quit(self):
        """removes this object from the DBUS connection and exits"""
        self.remove_from_connection()
        Gtk.main_quit()
        return

DBusGMainLoop(set_as_default=True)
myservice = MyDBUSService()
Gtk.main()

