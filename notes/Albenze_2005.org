#+TITLE: Molecular dynamics simulations of the solid phase epitaxy of Si: Growth mechanism and orientation effects
#+STARTUP: indent

[[cite:&Albenze_2005]]
[[../xopp/Al
Construction of SW115 melting temperature: 1660K Table 3 + SPE Speed.

Modification of the 3 body therm by increasing it to 115%

*Results of this work indicate that Tersoff is better in predicting the speed*

* Procedure 
*First*, generating the amorphous phase.
1. Melting: 3k5 K 10 ps
2. Quenching: ramp down 10K/ps (to?)

*Second*, checking the quality. Final distribution statistics in the amorphous part of the sample:
- peak of second neighbors in the structure factor
- Gaussian-like angular distribution

*Third* time to measure the SPE. Several thermostats in the range 1700-2500K; upper bound is defined by the melting temperature, Lower bound is defined by computational limits. Bottom atoms of the sample are frozen.
- PBC only in 2 directions
- velocity is computed using Mattoni Colombo (TODO: implement) method

*  Comments on regrowth
- 2000/2500 K Interface remains abrupt during annealing.
- Below 2000K Interface depend from the lattice orientation. *sometimes there are defects* Studied regrowth [100], [110], [111]

Regrow velocity is estimated and barriers are obtained using an Arrhenius formula.

*Defects*: They comments on some defects. But this will come in a second moment. But *an important point* is being able of confirming or neglecting the Drosd Washburn mechanism.
* Citation 
- 16: [[cite:&Brambilla_2000]] 
- 20: [[cite:&Keblinski_2002]]
- 26: [[cite:&Justo_1998]] 
# -
