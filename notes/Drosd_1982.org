#+TITLE: Some observations on the amorphous to crystalline transformation in silicon

[[../xopp/Drosd_1982.xopp]]

One of the first model for SPE formation.

* Bibliography
- [[cite:&Drosd_1980]] - where the data for the model are taken from.
