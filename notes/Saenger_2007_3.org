#+TITLE: Mask-edge defects in hybrid orientation direct-Si-bonded substrates recrystallized by solid phase epitaxy after patterned amorphization

[[cite:&Saenger_2007_1]] 
[[cite:&Saenger_2007_2]] 
[[cite:&Saenger_2007_3]] 
