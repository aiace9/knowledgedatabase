#+TITLE: Pressure-enhanced crystallization kinetics of amorphous Si and Ge: Implications for point-defect mechanisms

[[cite:&Lu_1991]]
[[../xopp/Lu_1991.xopp]]

models: 2, 12-17.
* citations
- 2: [[cite:&Csepregi_1977;&Csepregi_1978]] -78 is better Spaepen helped.
- 6: [[cite:&Suni_1982]]
- 12: [[cite:&Spaepen_1979]] - original model
- 13: [[cite:&Williams_1985;&Williams_1984]]  Second is better.
- 14: [[cite:&Narayan_1982]] - model + pyramids/undulation
- 15:
- 16: [[cite:&Pantelides_1988]]  
- 17: [[cite:&Licoppe_1986]] 
- 39: [[cite:&Roth_1990]] 
- 75:
# 
