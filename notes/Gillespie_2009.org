#+TITLE: Notes on: Gillespie, B., & Wadley, H. (2009): Atomistic examinations of the solid-phase epitaxial growth of silicon

- [[../xopp/Gillespie_2009.xopp]]
- [[cite:&Gillespie_2009]]
small error on figure: [[cite:&Gillespie_2010]]

They argue that jumps are due to interstitial elimination beyond the interface. 
