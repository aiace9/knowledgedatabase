#+TITLE: Vibrational analysis of silicon nanoparticles using simulation and decomposition of raman spectra

- [[../xopp/Povarnitsyn_2020.xopp]]
- [[cite:&Povarnitsyn_2020]] 

The scale of fig 1 is rather odd.

[[cite:&Nemanich_1989;&Tan_2011]]

Both place the couple of peaks as I would expect in the amorphous case. And also I obtain results that are reasonably similar to those reference. In figure ref:fig:different_vdos we observe two broad peaks for the SW potential at approximately 20 and 65 meV that correspond to 160 and 524 cm-1.

#+CAPTION: Average v-dos for different configurations ~10/20 deepending on the potential for a 216/1728 atoms system. More details in Silicon project, report for the amorphous characterization.
#+NAME: fig:different_vdos
#+ATTR_ORG: :width 600px
#+ATTR_LATEX: :width 0.9\linewidth
[[./figs/original_figures/0.png]]

In the end this article focus only on the Nanoparticles, and we don't really care much about them.

It is interesting instead the material on the models to obtain the raman spectrum starting from the v-DOS: the bond polarizability model.


* Relevant bibliography
- 34: [[cite:&Zotov_1999]] 
- 35: [[cite:&Thonhauser_2005]] 
- 39: [[cite:&Ivanda_2006]]
- 40: [[cite:&Lamb_1881]] "Original" study on the phonons of spherical nanoparticles (in general) It is very complicated.
    
    
