#+TITLE: The melting temperature of bulk silicon from ab initio molecular dynamics simulations

T_m calculation at constant entropy and constant pressure(NPH)

- how? :: theoretical
  - PBE
  - Solid/Liquid Interface
  - 256 atoms in box (128 l-Si + 128 c-Si) 11.3 x 11.3 x 40 A
  - SW + 1ps DFT MD (for sample preparation)
- results :: T_m = 1540 \pm 50
  (low accuracy due to short simulation time)

  
Cited results for $T_m$
- 1350 \pm 100 K LDA: [[cite:&Sugino_1995]]
- 1492 \pm 50  K GGA
- ~1691 K SW
- 1685 K exp
