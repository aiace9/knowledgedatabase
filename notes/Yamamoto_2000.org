#+TITLE: Replica-exchange molecular dynamics simulation for supercooled liquids

[[cite:&Yamamoto_2000]] Apply replica-exchange(RX) method to MD for detailed math of the method look [[articles on the same topic][here]]
    
  The aim is to generate *Canonical distributions* faster
- why? ::
  - speed up MD in glass systems

- Notes ::
  - they show the obtained canonical distribution function.
  - they show the walks of the subsystem in temperature
    
* articles on the same topic
(sorted by relevance)
1. cite:Sugita_1999
2. cite:Hukushima_1996, replica exchange for MC
3. cite:Hukushima_1998, replica exchange for MC
