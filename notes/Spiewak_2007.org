#+TITLE: Ab initio calculation of the formation energy of charged vacancies in germanium

[[../xopp/Spiewak_2007.xopp]]

*VASP* code

DFT + U for Ge and vacancy

Value of U = 1.9 eV -

In their calculations of alat

| dft           | alat (A) |
|---------------+----------|
| experiments   |   5.6569 |
|---------------+----------|
| LDA           |   5.6464 |
| LDA + d(orbs) |   5.6131 |
| LDA + U       |   5.5883 |

This means that d orbitals tend to reduce the alat (maybe because their presence
start to open the band gap). LDA + U further move in this direction delocalizing
the *d orbitals* and then increasing the s energetic.



#  LocalWords:  alat eV
