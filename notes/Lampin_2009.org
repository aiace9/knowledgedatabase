#+TITLE: Molecular dynamics simulations of the solid phase epitaxy of si: Growth mechanism and orientation effects

[[cite:&Lampin_2009]]

This paper studies SPE with [[cite:&Tersoff_1988_b]]. Studied regrowth along [100], [110], [111]. Regrow velocity is estimated and barriers are obtained using an Arrhenius formula.

* Introduction
*SPE* := solid phase epitax

Experimental info:
 - velocity of regrowth was estimate
 - micrometric thickness was used
 - 100 orientation is the most common
   
* Method:
*Former work* [[cite:&Krzeminski_2007]] 

Bond switching method to create amorphous

Tersoff and SW115 [[cite:&Albenze_2005]] are the only potential that do not melt the amorphous.

Notes:
- /Tersoff/ melting temperature: *2500K* (approx)
- SW115 melting temperature: 1650K (approx) see. cite:Albenze_2005.
  
*Results of this work indicate that Tersoff is better in predicting the speed*

* Procedure

*Sample creation*

1. First, generating the amorphous phase.
2. Melting: 3k5 K 10 ps
3. Quenching: ramp down 10K/ps (to?)
4. Checking the quality of a-Si
5. Final distribution statistics:
   - g(r) in the amorphous part of the sample:
   - angular distributions
   
*SPE procedure*
     
At this point is time to measure the SPE. Several thermostats in the range 1700-2500K; upper bound is defined by the melting temperature, Lower bound is defined by computational limits.

Sample structure: bottom atoms of the sample are frozen and 
PBC are applied only in 2 directions.
  
SPE velocity is computed using Mattoni /et al./ [[cite:&Mattoni_2004]] method.

* Results

- at 2000/2500 K: Interface remains abrupt during annealing
- Below 2000K : Interface depend from the lattice orientation. *sometimes there are defects*


Defects: They comments on some defects. But this will come in a second moment. An *important point* is being able of confirming or neglecting the Drosd  [[cite:&Drosd_1982]] mechanism

* Citation  
- 3: [[cite:&Drosd_1982]] 
