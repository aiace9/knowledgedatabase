#+TITLE: Notes on: Ku, W., & Eguiluz, A. G. (2002): Band-gap problem in semiconductors revisited: effects of core states and many-body self-consistency

[[../xopp/Ku_2002.xopp]]

Another GW calculation for Ge

Here the GW is exploited Self consistently.

So the LDA predict a < 0 band gap. Confirming that DFT does not seems to do a
good job. (Table 2)

GW applied self consistently can indeed open the band gap.

There are some observation for Silicon that are useful:
#+begin_quote
1. The *core electrons shield the attractive field of the nuclei*,
   thereby *raising the energy of the valence and conduction states*;
2. the *exchange* process partially *compensates* for this effect;
3. the states across the gap have different amplitudes in the core region;
   these amplitudes control the strength of the exchange. It is the larger
   lowering of the energy of the QP states below the gap, relative to those
   above it, due to the nonlocal core-valence exchange process, that leads to
   this novel all-electron effect.  The relativistic *2P states* contribute more
   than 90% to the opening of the Si gap, as they extend more in space, and have
   similar angular momentum character as the states that define the gap
#+end_quote

So our exchange can be over-compensating the attraction. But this is just an
assumption that is not backed.
