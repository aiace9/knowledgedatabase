#+TITLE: The configurational energy gap between amorphous and crystalline silicon

[[../xopp/Kail_2011.xopp]]

Cited by [[cite:&Drabold_2011]] as main reference, but also important in [[cite:&Pedersen_2017]]  

* Citations 
- [[cite:&Roura_2006]] - ~1eV/Dangling bond.
  I haven't read it so I have no idea how did they got this number
# 
