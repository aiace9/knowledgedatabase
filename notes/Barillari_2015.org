#+TITLE: Openbis ELN-LIMS: an open-source database for academic laboratories

- Laboratory Information Management System (LIMS)
ELN-LIMS based on the open source plat-
form openBIS [[cite:&Bauch_2011]].
. This platform is pre-set with sensible defaults and ready for use.
