#+TITLE: Planning identity management red hat enterprise linux 9-red hat customer portal

- [[cite:&planning_identity_management_mar2023]]
- [[../xopp/planning_identity_management_mar2023.xopp]]

IdP: identity provider eg keycloak

- identity stores
- authentication
- policies
- authorization policies

SSO at infrastructure level: kerberos with services like SSH, LDAP, NFS, DNS
SSO in web application: Keycloak(IdP)

Relevant outer citations:

- [[cite:&managing_certificates_in_idm_apr2023]]
- [[cite:&performing_disaster_recovery_with_identity_management_apr2023]]
