#+TITLE: 

   *what do they do?*
   Network potentials for copper
   *results:*
   A potential that well describe copper sufraces phenomena
   *how?*
   /theoretical/
   /experimental/
   *why?*
   *comments*
   - The descriptor is used in a different way, is not like a G(r) but
     the eta value is changed
   - The descriptor is composed by 8 radial parts + 43 angular parts,
     for a total 51, that is a small number.
   - The interaction radius is of *6A* that is big wrt our cutoff.
   - The network is small (30:30:1) the accuracy saturate around this
     shape (2492 parameters)
   - The final DFT data set:
     - 15k bulk structures
     - 13k slabs
     - 8k clusters of variable
     - *617k environments*
     - $\approx$ 17 atoms/cell, max 100
     - train = 33k structures
     - validation = 3k structures
   - Accuracy ball park
     - RMSEs: 3.6/3.9 meV per atom
     - RMSEs: 79.3/80.8 meV/A
     - MAEs: 2.09/2.22 meV per atom,
     - MAEs: 55.3/55.5 meV/A
* POI
  Chose the DFT points that need to be computed To achieve that they trained
  several network with *different architectures* to grant *different functional
  forms*. Then they run an MD starting form a point.  They evaluate the MD with
  the different potentials and add only those points that are predicted very
  differently.
