#+TITLE: Kinetics of solid phase crystallization in amorphous silicon
#+STARTUP: overview
#+STARTUP: indent

[[../xopp/Olson_1988.xopp]]
[[../graphs_digitalization/Olson_1988]]

[[cite:&Olson_1988]]

* Analyzing their introduction
** paragraph 1
   What are we talking about? (this is not intended to explain to the reader
   what are we talking about, just to state it! we talk about SPE. Do you know
   what is it? yes. good you don't? I will later explain. For now you should
   just know that it is important)
   
   Why is it important?
   
   - SPE can be called -- epitaxial reordering
   - First observation was in 1968, then a lot of
     research was done to understand the mechanisms
     behind this transformation
   - What were the motivations behind this work?
     - technological importance
     - other stuffs
** paragraph 2
   which are the phenomena that we know can happen? And what
   novelty can we use to exploit to look at this with
   new eyes?
   
   - The heating of a-Si can trigger many phenomena
   - which are are they?
   - Now they can be studied with a new set of
     techniques
   - these techniques provide much more accuracy
** paragraph 3
   What will this paper talk about?
   This paper will:
   - review the recent progress in what?
     - a-Si to c-Si phase transition characterization
     - at high temperature
   - examine the phenomena in different conditions
     providing new insight
** paragraph 4
   Why these phenomena are present? (aka why a-Si tend to crystallize) Which is
   the energetic advantage behind the crystallization process? how does this
   affect the geometry?
   
   This paragraph is composed by 7 sentences.
   
   28 + 27 + 18 + 14 + 23 + 32 + 21 = 163 words

   Perfect metric.
** paragraph 5
   a-Si structure is a key point in understanding the crystallization. what do
   we know about it experimentally? Do we have any insight about why is that so?
** paragraph 6
   explaining the figure. Who did it? What method did he use?
   - What is the insight that the figure give us?
** paragraph 7
   This paragraph can be optimize but
   - We saw that a-Si can become c-Si in paragraph 6, now:
   - How does it happens? Trough RNG
   - but this has a limitation, we will never get a perfect crystal
   - to overcome this we need to build on top of a template
** paragraph 8
   Finally SPE, cite some pioneering work and recap where did they arrive.  Our
   work focus on something right? is it a subsection of what they did?

   (in this case they stet what [[cite:&Csepregi_1978]] did and comment on why this
   work is only analyzing the <100> direction)
** Paragraph 9
   (we understood that a-Si is important and how it is prepared makes up a
   great deal)
   
   How can a-Si be prepared?
** paragraph 10
   Two of the 6 methods to realize a-Si are introduced, and final notes are made
   on which are the differences.
   
** paragraph 11
   The effects of impurities is summarized
** paragraph 11
   The effects of high impurity concentration is summarized
** paragraph 12
   shine attention to a new problem:
   - a-Si crystallization in the presence of dopants goes trough different physical and
     chemical process.
   - There is the need for
* Activated process
\begin{equation}
  v = v_0 exp(-\frac{E_a}{k_b T})
\end{equation}

Where (this work):
  - Si+ implanted :: 2.68 \pm 0.05 eV, 2.1e8 cm/s
  - Si evaporated :: 2.71 \pm 0.05 eV, 2.3e8 cm/s
  - [[cite:&Csepregi_1978]] :: 2.3 \pm 0.1 eV, 2.8e6 cm/s
  
  $k_b$ = 8.6173e-5 ev/K
* Tables
|    T | cm/sec |  A/ps | time[ns]/cell |
|------+--------+-------+---------------|
| 1300 |   8e-1 |  8e-5 |      6.79e+01 |
| 1200 |   1e-1 |  1e-5 |      5.43e+02 |
|------+--------+-------+---------------|
| 1000 |   9e-3 |  9e-7 |      6.03e+03 |
|  900 |   1e-3 |  1e-7 |      5.43e+04 |
|  500 |   2e-9 | 2e-13 |      2.72e+10 |
#+TBLFM: $3=$2*1e-4 :: $4=5.43/$3/1000;%2.2e

|   | Ea[eV]       | v0 cm/s |
|---+--------------+---------|
| 1 | 2.68 \pm .05 |   3.1e8 |
| 2 | 2.71 \pm .05 |   2.3e8 |
|---+--------------+---------|
| 3 | 2.3 \pm .1   |   2.8e6 |
| 5 | 2.85 \pm     |         |
|---+--------------+---------|
| 4 | 2.7 \pm .2   |         |
|---+--------------+---------|

1. implanted this work
2. evaporated this wokr
3. implanted, cspepergi
4. evaporated, cspepergi
5. [[cite:&Lietoila_1982]]

thermal diffusion lengths are > 1 um. 
* References
- 2: [[cite:&Biegelsen_1982]]
- 29: [[cite:&Lietoila_1982]] 
- 26: [[cite:&Williams_1984]] 
- 63: [[cite:&Olson_1982]]
  
