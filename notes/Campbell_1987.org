#+TITLE: Light trapping properties of pyramidally textured surfaces
- original paper on pyramids
- 
* main points
    **what do they do?**
    - propose ideas on different pattern for surface
   
    **how?**
    - theoretical/computational refraction study, pure optics
    - silicon optical properties from ref11
   
    **why?**
    - maximize photon absorption in solar cells.

* POI
    - the pattern used in cite:Lee_2020 was found to be the best 
      one in this study perpendicular slat approach: perpendicular slat
    - Tile's pattern is also good with regular pyramids
    - they computed the short circuit current (NO IDEA HOW)
    - d = 17.09, W = 21.14 d/w = .808
    - w = 100 um => d = 80um 
