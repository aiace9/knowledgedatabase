#+TITLE: Ultrafast recombination and trapping in amorphous silicon


They enstablish the experimental relation

\begin{equation}
\frac{dN}{dt} = \gamma N^2
\end{equation}

where N is the number of electron hole pairs created by excitation.
- $\gamma$ approximately 7e-9 cm^3/sec for a-Si:H
- $\gamma$ approximately 5e-9 cm^3/sec for a-Si

This little difference in gamma between 2 materials that are so different should
justify the independence of this measure from the microscopical realization.
