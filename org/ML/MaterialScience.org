#+TITLE:ML for Material Science
#+STARTUP: overview
#+STARTUP: indent

- [[cite:&Zuo_2020]] - performance between methods
- [[cite:&Kocer_2019]] - BP vs SOAP in silicon
- [[cite:&Pellegrini_feb2023]] - Novel architecture, TODO read

* Descriptors
- [[cite:&Steinhardt_1983]]
- [[cite:&Maras_2016]] - Ovito automatic classification
- [[cite:&Gastegger_2018]]
- [[cite:&Smith_2017]] - ANI
- [[cite:&Bartok_2013]] - SOAP
- [[cite:&Drautz_2019]] - ACE
- [[cite:&Valle_2010;&Oganov_2009;&Valle_2008]] - Fingerprint
- [[cite:&Imbalzano_2018]] - automatic features selection in BP 
* GAP
- [[cite:&Bartok_2010]] - First Si pot
- [[cite:&Bartok_2018]] - Silicon pot.
* ACE
- [[cite:&Lysogorskiy_2021]] -Silicon pot

* Neural Network
- [[cite:&Zuo_2020]]
- [[cite:&Behler_2011]]
- [[cite:&Behler_2007]]
- [[cite:&Lot_2020]]
- [[cite:&Liu_2017]] - for Raman spectra

* Data set optimization for NN
- [[cite:&Browning_2017]]
- [[cite:&Artrith_2012]]
- [[cite:&Artrith_2016]]
- [[cite:&Artrith_2018]]
- [[cite:&Herr_2018]] - meta-dynamic 
* graph networks
- [[cite:&Schutt_2018]] - schnet
* Markov
(this section is more interesting for future SPE)
- [[cite:&Conrad_2016]]
- [[cite:&Bapst_2020]]
- [[cite:&Schutte_2011]] - Markov modeling
- [[cite:&Pellegrini_2016]]

* Reviews
- [[cite:&Schmidt_2019]]
- [[cite:&Butler_2018]] 
* Not physics related
- [[cite:&Maaten_2008]] - t-SNE
- [[cite:&Kornblith_2019]] -similarities between NN
* Active learning
- [[cite:&Podryabinkin_2017]]
* Codes
- [[cite:&Lot_2020]]
- [[cite:&tensorflow_2015]] - TF
- [[cite:&Dean_2012]] - on TF parallelization
- [[cite:&Kingma_2014_ADAM]] - ADAM
* Primitive tentative
- [[cite:&Gassner_1998]]

* Long range interaction
- [[cite:&Jacobson_2021]]
