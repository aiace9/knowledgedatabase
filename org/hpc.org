#+TITLE: HPC
#+STARTUP: overview
#+STARTUP: indent

* Slrum
- [[cite:&slurmdoc_mpiguide_oct2022]]
** Accounting (dbd)
- [A] [[cite:&slurmdoc_ResourceLimits_jan2023]]
- [A] [[cite:&slurmdoc_MultifactorPriorityPlugin_mar2023]]
- [[cite:&slurmdoc_accounting_dec2022]]
- [[cite:&slurmdoc_Fairshare_mar2023]]
- you will need [[./linux.org::Maria db][Maria db]]
** PAM
https://slurm.schedmd.com/pam_slurm_adopt.html - TODO: read

Suggested solution is suboptimal. It needs also some twiking with cgroups.
* HPC/theory
- [[cite:&Dongarra_2003]]
- [[cite:&Dongarra_1998]]
* Performance
- [[cite:&Velten_2022]] - epyc ROME + chip structure
- [[cite:&Xing_2014]] - performance test (example)
* Processors
- [[cite:&xeon_thermal_guide_2019]]
* Containers
- [[cite:&Beltre_2019]]
- [[cite:&Grant_2008]] (com)
- [C] [[cite:&Balaji_2002]] (RDMA)
* MLAG
- [A] [[cite:&Geslinux_2012]] - VLT
- [[cite:&MLAG_MikroTik_RouterOS_2023]]
- [[cite:&Fratto_2011]] 
- [[cite:&Bhagat_MLAG_2020]]
- [[cite:&Allan_2012]] - SPB
* Data center design
- [A] [[cite:&ANSI-TIA942_2005]] 
- [C] [[cite:&Shahrani_2019]]
- [[cite:&DellS5200_ON]] - switch
* Ansible
- [[https://github.com/stackhpc/ansible-slurm-appliance/tree/main/ansible][stack hpc]]
- [[https://github.com/NVIDIA/deepops/tree/release-22.04][NVIDIA]]  
