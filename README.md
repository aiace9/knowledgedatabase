# Bibliography

References, notes, and relations between articles that I use in my research.

Covered topics are:
- creation of machine learning potentials,
- density functional theory and molecular dynamic simulations,
- silicon and germanium properties.

Data are optimized to work with the
[org-ref](https://github.com/jkitchin/org-ref) package.

# Project organization

The folders inside this project are organized as follows:

- notes: collection of org files containing notes on the particular article
- pdf/djvu: a folder containing all the article's pdf/djvu (available upon request)
- xopp: a folder containing all hand written notes with
  [xournalpp](https://github.com/xournalpp/xournalpp).  Files are in Gzip
  format. Unfortunately the location of the pdf used as background is stored as
  absolute path.
- supplementary_material: supplementary material for selected articles
- graph_digitalization: graphs screenshot, related CSVs and scripts to read them.
  The digitalization is obtained with [EasyNdData](http://puwer.web.cern.ch/puwer/EasyNData)
- org: org files organizing citation per topic, the idea is to move this in to tags.

# Spacemacs configuration

`bibtex` is a required layer, and  in the `user-cofig` section you need to add:

```lisp
  ;; Set variables for org-ref
  (setq bibtex-completion-bibliography '("path_to/Bibliography/references.bib")
        bibtex-completion-library-path '("path_to/Bibliography/pdf/")
        bibtex-completion-notes-path "path_to/Bibliography/notes/"
        bibtex-completion-pdf-open-function
        (lambda (fpath)
          (call-process "xdg-open" nil 0 nil fpath)))
```

`xdg-open` command must be available from the terminal and properly configured. If
you are not running a desktop environment
[DE](https://wiki.archlinux.org/title/Desktop_environment) you may want to
install a package that provides `mimetype`.

# Org-mode better defaults

Optionally you can also add a default application to open `xopp` files, and render
latex equations larger (this is very useful for screen with high dpi).

```lisp
  (spacemacs|use-package-add-hook org
    :post-config
    (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.3))
    (add-to-list 'org-file-apps '("\\.xopp\\'" . "xournalpp %s"))
  )
```
